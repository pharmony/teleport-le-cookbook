# frozen_string_literal: true

#
# The fields marked with a "[Mandatory]" must be field in in order to get a
# working installation.
#

# [Mandatory] Contact information, default empty. Set to mailto:your@email.com
default['teleport_le']['email'] = []

# Use the Let's encrypt's staging environment.
# This allow you to test your configuration before to use the production mode
# which is limited in the amount of time you're allowed to request certificates.
default['teleport_le']['staging'] = false

# Days before the certificate expires at which the certificate will be renewed
default['teleport_le']['renew_delay'] = 30

# Filter the hosts where to request for an SSL certificate.
default['teleport_le']['only_hostnames'] = []
