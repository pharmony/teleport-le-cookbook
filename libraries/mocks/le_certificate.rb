# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt certificate resource
    #
    module LeCertificate
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_certificate_url(acme_client_certificate_id)
        ).with(
          acme_client_headers(
            accept: 'application/pem-certificate-chain',
            content_type: 'application/jose+json'
          )
        ).to_return(
          status: 200,
          body: '',
          headers: response_headers
        )
      end

      def self.response_headers
        {
          'Content-Type': 'application/pem-certificate-chain'
        }
      end
    end
  end
end
