# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt new nonce resource
    #
    module LeNewNonce
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :head,
          acme_client_new_nonce_url
        ).with(
          acme_client_headers(no_encoding: true)
        ).to_return(
          status: 200,
          body: '',
          headers: response_headers
        )
      end

      def self.response_headers
        {
          'Replay-Nonce': 'oFvnlFP1wIhRlYS2jTaXbA'
        }
      end
    end
  end
end
