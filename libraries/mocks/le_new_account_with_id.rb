# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt new account resource
    #
    module LeNewAccountWithId
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_new_account_url(acme_client_account_id)
        ).with(
          acme_client_headers(content_type: 'application/jose+json')
        ).to_return(
          status: 201,
          body: '',
          headers: {}
        )
      end
    end
  end
end
