# frozen_string_literal: true

module TeleportLeCookbook
  module Mocks
    #
    # Shared methods across mocks
    #
    module Commons
      def acme_client_account_id
        'evOfKhNU60wg'
      end

      def acme_client_auth_id
        'PAniVnsZcis'
      end

      def acme_client_authorization_url(id)
        "#{acme_client_base_url}/acme/authz/#{id}"
      end

      def acme_client_base_url
        'https://acme-v02.api.letsencrypt.org'
      end

      def acme_client_certificate_id
        'mAt3xBGaobw'
      end

      def acme_client_certificate_url(id)
        "#{acme_client_base_url}/acme/cert/#{id}"
      end

      def acme_client_challenge_id
        'prV_B7yEyA4'
      end

      def acme_client_challenge_url(id)
        "#{acme_client_base_url}/acme/chall/#{id}"
      end

      def acme_client_finalize_id
        'TOlocE8rfgo'
      end

      def acme_client_finalize_url(id)
        "#{acme_client_base_url}/acme/order/#{id}/finalize"
      end

      def acme_client_headers(options = {})
        headers = {
          headers: {
            'Accept' => options[:accept] || '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent' => user_agent
          }
        }

        headers[:headers].delete('Accept-Encoding') if options[:no_encoding]

        if options[:content_type]
          headers[:headers]['Content-Type'] = options[:content_type]
        end

        headers
      end

      def acme_client_new_account_url(id = nil)
        "#{acme_client_base_url}/acme/new-acct#{"/#{id}" if id}"
      end

      def acme_client_new_nonce_url
        "#{acme_client_base_url}/acme/new-nonce"
      end

      def acme_client_new_order_url
        "#{acme_client_base_url}/acme/new-order"
      end

      def acme_client_order_url(id)
        "#{acme_client_base_url}/acme/order/#{id}"
      end

      def user_agent
        "Acme::Client v#{Acme::Client::VERSION} " \
        '(https://github.com/unixcharles/acme-client)'
      end
    end
  end
end

