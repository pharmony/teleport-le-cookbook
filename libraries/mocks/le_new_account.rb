# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt new account resource
    #
    module LeNewAccount
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_new_account_url
        ).with(
          acme_client_headers(content_type: 'application/jose+json')
        ).to_return(
          status: 201,
          body: response.to_json,
          headers: response_headers
        )
      end

      def self.response
        {
          status: 'valid',
          orders: acme_client_new_orders_url(acme_client_account_id)
        }
      end

      def self.response_headers
        {
          location: acme_client_new_account_url(acme_client_account_id)
        }
      end

      def self.acme_client_new_orders_url(id)
        "#{acme_client_base_url}/acme/new-acct/#{id}/orders"
      end
    end
  end
end
