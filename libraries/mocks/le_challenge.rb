# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt challenge resource
    #
    module LeChallenge
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_challenge_url(acme_client_challenge_id)
        ).with(
          acme_client_headers(content_type: 'application/jose+json')
        ).to_return(
          status: 200,
          body: response.to_json,
          headers: response_headers
        )
      end

      def self.response
        {
          type: 'http-01',
          url: acme_client_challenge_url(acme_client_challenge_id),
          status: 'valid',
          token: 'DGyRejmCefe7v4NfDGDKfA'
        }
      end

      def self.response_headers
        {
          'Content-Type': 'application/json'
        }
      end
    end
  end
end
