# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt authz resource
    #
    module LeAuthz
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_authorization_url(acme_client_auth_id)
        ).with(
          acme_client_headers(no_encoding: true)
        ).to_return(
          status: 200,
          body: response.to_json,
          headers: response_headers
        )
      end

      def self.response
        {
          status: 'pending',
          expires: Time.now + (1 * 60 * 60),
          identifier: {
            type: 'dns',
            value: 'www.example.org'
          },
          challenges: [
            {
              type: 'http-01',
              url: acme_client_challenge_url(acme_client_challenge_id),
              status: 'pending',
              token: 'DGyRejmCefe7v4NfDGDKfA'
            }
          ]
        }
      end

      def self.response_headers
        {
          'Content-Type': 'application/json'
        }
      end
    end
  end
end
