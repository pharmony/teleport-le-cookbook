# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt directory resource
    #
    module LeDirectory
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :get,
          "#{acme_client_base_url}/directory"
        ).with(
          acme_client_headers
        ).to_return(
          status: 200,
          body: response.to_json,
          headers: {}
        )
      end

      def self.response
        {
          newAccount: acme_client_new_account_url,
          newNonce: acme_client_new_nonce_url,
          newOrder: acme_client_new_order_url
        }
      end
    end
  end
end
