# frozen_string_literal: true

require_relative 'commons'

module TeleportLeCookbook
  module Mocks
    #
    # Mocks the Let's Encrypt finalize nonce resource
    #
    module LeFinalizeOrder
      extend Mocks::Commons

      def self.stub!
        WebMock.stub_request(
          :post,
          acme_client_finalize_url(acme_client_finalize_id)
        ).with(
          acme_client_headers(content_type: 'application/jose+json')
        ).to_return(
          status: 200,
          body: response.to_json,
          headers: response_headers
        )
      end

      def self.response
        {
          status: 'valid',
          authorizations: [
            acme_client_authorization_url(acme_client_auth_id)
          ],
          finalize: acme_client_finalize_url(acme_client_finalize_id),
          certificate: acme_client_certificate_url(acme_client_certificate_id)
        }
      end

      def self.response_headers
        {
          'Content-Type': 'application/json',
          location: acme_client_order_url(acme_client_account_id)
        }
      end
    end
  end
end
