# frozen_string_literal: true

require 'webmock'

module TeleportLeCookbook
  #
  # Mock collection uses only for testing
  #
  module Mocks
    #
    # Mocks the acme-client gem so that no real calls are sent to
    # the Let's Encrypt guys. :)
    #
    # Following the LE doc: https://www.dogtagpki.org/wiki/ACME_API#Creating_an_Account
    #
    def mock_acme_client!
      Mocks::LeDirectory.stub!
      Mocks::LeNewNonce.stub!
      Mocks::LeNewAccount.stub!
      Mocks::LeNewAccountWithId.stub!
      Mocks::LeNewOrder.stub!
      Mocks::LeAuthz.stub!
      Mocks::LeChallenge.stub!
      Mocks::LeFinalizeOrder.stub!
      Mocks::LeCertificate.stub!

      WebMock.enable!
    end
  end
end
