# Teleport LE Cookbook

The purpose of this cookbook is to install Let's Encrypt, generate a SSL certificate to be used by the teleport-le coobook.

## Supported Platforms

* Debian 9/10

_If you want to add more platforms, feel free to open a PR!_

## Attributes

Please look at the `attributes/teleport_le.rb` file.

## Usage

In order to use this cookbook:

1. Add the cookbook to your chef repository.
2. Add the `teleport-le::default` recipe to the `run_list` where it should run (must be the same as the teleport-ce one).
3. Converge ! :)

You can find a Policyfile example from [the teleport-ce README.md file](https://gitlab.com/pharmony/teleport-ce-cookbook#policyfile-example).
