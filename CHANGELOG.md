# Changelog

This file is used to list changes made in each version of this cookbook.

## Unreleased

 - Makes tests passing
 - Remove Debian Jessie support (apt is failing)
 - Adds Debian Buster
 - Allows filtering the nodes where to run this cookbook via the `only_hostnames` attribute

# 1.0.0 (2019-11-12)

 - Initial release.
