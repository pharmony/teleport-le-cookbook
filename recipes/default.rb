# frozen_string_literal: true

#
# Cookbook:: teleport_le-cookbook
# Recipe:: default
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

if node['teleport_le']['only_hostnames'].empty?
  Chef::Log.info 'Teleport LE: Provisionning the node ...'
  include_recipe 'teleport-le::request_cert'
else
  Chef::Log.info 'Teleport LE: Checking if the node should be provisionned ...'
  Chef::Log.info "Teleport LE: node['teleport_le']['only_hostnames']: #{node['teleport_le']['only_hostnames'].inspect}"
  Chef::Log.info "Teleport LE: node['hostname']: #{node['hostname'].inspect}"
  if node['teleport_le']['only_hostnames'].include?(node['hostname'])
    Chef::Log.warn "Teleport LE: #{node['hostname']} is included in the " \
                   'only_hostnames list, therefore the host will be ' \
                   'provisionned.'
    include_recipe 'teleport-le::request_cert'
  else
    Chef::Log.info "Teleport LE: #{node['hostname']} is not included in the " \
                   'only_hostnames list, therefore the host will be ' \
                   'provisionning it ...'
  end
end
