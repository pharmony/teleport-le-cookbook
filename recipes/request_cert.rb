# frozen_string_literal: true

#
# Cookbook:: teleport_le-cookbook
# Recipe:: default
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

# `mock_acme_client!`
Chef::Recipe.send(:include, TeleportLeCookbook::Mocks)

directory '/etc/ssl' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

nginx_base_path = '/var/www'

directory nginx_base_path do
  owner 'www-data'
  group 'www-data'
  mode '0755'
  action :create
end

unless node.key?('teleport')
  Chef::Log.error 'The Teleport LE cookbook requires the Teleport CE ' \
                  'cookbook config.'
  Chef::Log.error "The cookbook tried to access the node['teleport'] config " \
                  'but it is not present.'
  raise
end
unless node['teleport']['auth_service']
  Chef::Log.error 'The Teleport LE cookbook requires the Teleport CE ' \
                  'cookbook auth_service config.'
  Chef::Log.error "The cookbook tried to access the ' \
                  'node['teleport']['auth_service'] config " \
                  'but it is not present.'
  raise
end
if node['teleport']['auth_service']['public_addr'].nil? ||
   node['teleport']['auth_service']['public_addr'].empty?
  reason = if node['teleport']['auth_service']['public_addr'].nil?
             'no present'
           else
             'empty'
           end
  Chef::Log.error 'The Teleport LE cookbook requires the Teleport CE ' \
                  'cookbook public_addr config.'
  Chef::Log.error 'The cookbook tried to access the ' \
                  "node['teleport']['auth_service']['public_addr'] config " \
                  "but it is #{reason}."
  raise
end

teleport_public_addr = node['teleport']['auth_service']['public_addr']

public_host = teleport_public_addr

if teleport_public_addr.include?(':')
  public_host = teleport_public_addr.split(':').first
end

nginx_teleport_path = "#{nginx_base_path}/#{public_host}"

directory nginx_teleport_path do
  owner 'www-data'
  group 'www-data'
  mode '0755'
  action :create
end

#
# Nginx cookbook configuration
#
# Install
nginx_install 'default' do
  source 'distro'
end
# Enable it
nginx_site public_host do
  template 'teleport.erb'
  cookbook 'teleport-le'
  variables public_host: public_host
end

#
# Acme cookbook configuration
#
include_recipe 'acme'

node.normal['acme']['contact'] = node['teleport_le']['email']

staging = node['teleport_le']['staging'] ? 'staging-' : ''

ruby_block 'Display warning for staging env' do
  action :run
  block do
    if staging
      Chef::Log.warn "Teleport LE: Using the staging Let's Encrypt environment"
    end
  end
end

acme_dir = "https://acme-#{staging}v02.api.letsencrypt.org/directory"
node.normal['acme']['dir'] = acme_dir

node.normal['acme']['renew'] = node['teleport_le']['renew_delay']

teleport_host = teleport_public_addr.split(':').first

ruby_block 'Show host used for LE cert' do
  action :run
  block do
    Chef::Log.info 'Teleport LE: Requesting certificate for ' \
                   "#{teleport_host} ..."
  end
end

crt_path = "/etc/ssl/#{teleport_host}.crt"
key_path = "/etc/ssl/#{teleport_host}.key"

# Mocks the acme-client gem when running with Kitchen CI
mock_acme_client! if ENV['TEST_KITCHEN'] == '1'

acme_certificate teleport_host do
  crt crt_path
  key key_path
  wwwroot nginx_teleport_path
end

#
# Teleport CE cookbook configuration
#
node.normal['teleport']['proxy_service']['https_key_file'] = key_path
node.normal['teleport']['proxy_service']['https_cert_file'] = crt_path
Chef::Log.info 'Teleport LE: Teleport attributes updated to incude produced ' \
               'SSL certificate!'
