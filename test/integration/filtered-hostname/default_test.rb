# frozen_string_literal: true

# Inspec test for recipe teleport-ce-cookbook::node

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe directory('/var/www/teleport-le.cookbooks.chef') do
  it { should_not exist }
end

describe package('nginx') do
  it { should_not be_installed }
end

describe file('/etc/nginx/sites-enabled/teleport-le.cookbooks.chef') do
  it { should_not exist }
end

# The teleport SSL certificate should be generated
describe file('/etc/ssl/teleport-le.cookbooks.chef.crt') do
  it { should_not exist }
end
describe file('/etc/ssl/teleport-le.cookbooks.chef.key') do
  it { should_not exist }
end

#
# Testing node attributes using the `node-export` cookbook which exports the
# node attributes to a JSON file.
#
describe file('/tmp/chef_node.json') do
  it { should be_a_file }

  its(:content) {
    should match %r{
      "teleport":{
        "auth_service":{
          "public_addr":"teleport-le.cookbooks.chef"
        }
      }
    }x
  }
end
