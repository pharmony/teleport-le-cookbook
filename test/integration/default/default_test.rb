# frozen_string_literal: true

# Inspec test for recipe teleport-ce-cookbook::node

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe directory('/var/www/teleport-le.cookbooks.chef') do
  it { should exist }
  its('owner') { should eq 'www-data' }
  its('group') { should eq 'www-data' }
  its('mode') { should cmp '0755' }
end

describe package('nginx') do
  it { should be_installed }
end

describe file('/etc/nginx/sites-enabled/teleport-le.cookbooks.chef') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
end

# The teleport SSL certificate should be generated
describe file('/etc/ssl/teleport-le.cookbooks.chef.crt') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
end
describe file('/etc/ssl/teleport-le.cookbooks.chef.key') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0400' }
end

#
# Testing node attributes using the `node-export` cookbook which exports the
# node attributes to a JSON file.
#
describe file('/tmp/chef_node.json') do
  it { should be_a_file }

  its(:content) {
    should match %r{
      "teleport":{
        "auth_service":{
          "public_addr":"teleport-le.cookbooks.chef"
        },
        "proxy_service":{
          "https_key_file":"/etc/ssl/teleport-le.cookbooks.chef.key",
          "https_cert_file":"/etc/ssl/teleport-le.cookbooks.chef.crt"
        }
      }
    }x
  }
end
